# AUTHORS

José Gilberto RESÉNDIZ FONSECA, gil7hero@gmail.com, Intern GEOLOC, IFSTTAR / University Gustave Eiffel

Miguel ORTIZ, miguel.ortiz@univ-eiffel.fr, Engineer GEOLOC, IFSTTAR / University Gustave Eiffel

Valérie RENAUDIN, valerie.renaudin@univ-eiffel.fr, Head of GEOLOC, IFSTTAR / University Gustave Eiffel


# CONTRIBUTORS

Céline RAGOIN, celine.ragoin@univ-eiffel.fr, IT development manager of GEOLOC, IFSTTAR / University Gustave Eiffel

Antoine GRENIER, antoine.grenier@laposte.net, Intern GEOLOC, IFSTTAR / University Gustave Eiffel

Nicolas ROELANDT, nicolas.roelandt@univ-eiffel.fr, Engineer AME, IFSTTAR / University Gustave Eiffel

Aravind RAMESH, aravind.ramesh@univ-eiffel.fr, Research Engineer, AME-GEOLOC, IFSTTAR / University Gustave Eiffel