package fr.ifsttar.geoloc.geoloclib.computations;

import android.location.GnssStatus;
import android.util.Log;

import org.ejml.simple.SimpleMatrix;
import org.gogpsproject.Constants;

import java.util.HashMap;
import java.util.Map;
import java.util.Vector;
import java.util.concurrent.ThreadLocalRandom;

import fr.ifsttar.geoloc.geoloclib.Coordinates;
import fr.ifsttar.geoloc.geoloclib.Options;
import fr.ifsttar.geoloc.geoloclib.Utils;
import fr.ifsttar.geoloc.geoloclib.satellites.GNSSObservation;
import fr.ifsttar.geoloc.geoloclib.satellites.SatellitePositionGNSS;

public class EnhancedLSE extends PVT
{
    private Map<String, GNSSObservation> MapSatelliteObservations;
    private SatellitePositionGNSS satellitePosition;
    private GNSSObservation satelliteObservation;

    private Coordinates position;
    private SimpleMatrix initX;

    private HashMap<Integer, Integer> hashMapSystemIndex;

    private Options processingOptions;

    // Constants
    private final double SIGMA_CODE_L1 = 5; // [m], precision "a priori" of a pseudorange measurement.

    private int MINIMAL_PARAM = 4;
    private int IDX_X = 0;
    private int IDX_Y = 1;
    private int IDX_Z = 2;
    private int IDX_C_DTR = 3;
    private double counter;

    //----------------------------------------------------------------------------------------------

    // Constructor 1
    public EnhancedLSE(Options processingOptions, SimpleMatrix initX, double epochCounter)
    {
        this.counter = epochCounter;
        this.initX = initX;
        this.processingOptions = processingOptions;
    }

    //----------------------------------------------------------------------------------------------

    // Function to refresh satellite observations
    public void refreshSatelliteObservations(HashMap<String, GNSSObservation> _satelliteObservations)
    {
        this.MapSatelliteObservations = _satelliteObservations;
        setCurrentSystems();
    }

    //----------------------------------------------------------------------------------------------

    // Function to Compute Position
    public void computePosition()
    {
        int nbParam = MINIMAL_PARAM + (hashMapSystemIndex.size() - 1);
        int nbObs = getNumberObservations();

        // Setting the x to create matrices with the right size afterwards
        X = new SimpleMatrix(nbParam, 1);
        X.set(IDX_X,0, initX.get(0));
        X.set(IDX_Y,0, initX.get(1));
        X.set(IDX_Z,0, initX.get(2));

        // Design Matrix
        SimpleMatrix A = new SimpleMatrix(nbObs, X.numRows());

        // Misclosure vector
        SimpleMatrix B = new SimpleMatrix(nbObs,1);

        // Weighting matrix
        SimpleMatrix Q = SimpleMatrix.identity(nbObs);

        double tow = 0;
        int loopcount = 0;

        do
        {
            position = new Coordinates(X.get(IDX_X,0), X.get(IDX_Y,0), X.get(IDX_Z,0));
            TropoCorrections tropoCorrections = new TropoCorrections(position);

            int i = 0;
            for(Map.Entry<String, GNSSObservation> entry : MapSatelliteObservations.entrySet())
            {
                Vector<EnhancedLSE.Measurements> measurementsVector;

                satelliteObservation = entry.getValue();

                satellitePosition = satelliteObservation.getSatellitePosition();

                tow = satelliteObservation.getTrx();

                if(satellitePosition == null)
                {
                    return;
                }

                double satX = satellitePosition.getSatCoordinates().getX();
                double satY = satellitePosition.getSatCoordinates().getY();
                double satZ = satellitePosition.getSatCoordinates().getZ();
                double satDt = satellitePosition.getDtSat();

                // compute tropspheric correction
                double tropoCorr = 0;
                if(processingOptions.isTropoEnabled())
                {
                    tropoCorr = tropoCorrections.getSaastamoinenCorrection(satellitePosition.getSatElevation());
                }

                measurementsVector = selectMeasurements(satelliteObservation);

                double geometricDistance = Utils.distanceBetweenCoordinates(
                        position,
                        satellitePosition.getSatCoordinates());

                for(EnhancedLSE.Measurements obs: measurementsVector)
                {
                    // Setting the weight
                    double sigmaMeas = getWeightValue(obs.cn0);

                    A.set(i, IDX_X, (X.get(IDX_X) - satX) / geometricDistance);
                    A.set(i, IDX_Y, (X.get(IDX_Y) - satY) / geometricDistance);
                    A.set(i, IDX_Z, (X.get(IDX_Z) - satZ) / geometricDistance);
                    A.set(i, IDX_C_DTR, 1);

                    // Checking if we have multiple system, and if satellite is part of the reference constellation
                    if (counter == 0)
                    {
                        if (hashMapSystemIndex.size() > 1
                                && hashMapSystemIndex.get(entry.getValue().getConstellation()) != IDX_C_DTR)
                        {
                            int idxGTO = hashMapSystemIndex.get(entry.getValue().getConstellation());

                            A.set(i, idxGTO, 1);
                            B.set(i, 0,
                                    obs.pseudo
                                            - geometricDistance
                                            - X.get(IDX_C_DTR)
                                            + Constants.SPEED_OF_LIGHT  * satDt
                                            - X.get(idxGTO) // System Time Offset
                                            - tropoCorr);
                        }
                        else
                        {
                            B.set(i, 0,
                                    obs.pseudo
                                            - geometricDistance
                                            - X.get(IDX_C_DTR)
                                            + Constants.SPEED_OF_LIGHT  * satDt
                                            - tropoCorr);
                        }
                    }
                    else
                    {
                        if (hashMapSystemIndex.size() > 1
                                && hashMapSystemIndex.get(entry.getValue().getConstellation()) != IDX_C_DTR)
                        {
                            int idxGTO = hashMapSystemIndex.get(entry.getValue().getConstellation());
                            A.set(i, idxGTO, 1);
                        }
                        double tmpCond1 = obs.pseudo - geometricDistance + Constants.SPEED_OF_LIGHT  * satDt
                                - tropoCorr;
                        double random = ThreadLocalRandom.current().nextDouble(0.004e-6, 0.01e-6);
                        double tmpCond2 = tmpCond1 + (random * Constants.SPEED_OF_LIGHT);
                        double matValB = Math.abs(tmpCond1 - tmpCond2);
                        B.set(i, 0, matValB);
                    }

                    Q.set(i,i, Math.pow(SIGMA_CODE_L1 * (sigmaMeas), 2));
                    i++;
                }
            }

            computeLSE(A, B, Q);
            loopcount ++;

        }while((loopcount < 25) && (dX.normF() > 10e-2));

        position = new Coordinates(X.get(IDX_X,0),X.get(IDX_Y,0),X.get(IDX_Z,0), tow);
    }

    //----------------------------------------------------------------------------------------------

    // Measurement Selection
    private Vector<EnhancedLSE.Measurements> selectMeasurements(GNSSObservation satelliteObservation)
    {
        Vector<EnhancedLSE.Measurements> measurementsList = new Vector<>();

        EnhancedLSE.Measurements meas;

        int codeL1;
        int codeL5;

        // Checking the constellation
        switch (satelliteObservation.getConstellation())
        {
            case GnssStatus.CONSTELLATION_GPS:
                codeL1 = Constants.CODE_L1C;
                codeL5 = Constants.CODE_L5X;
                break;
            case GnssStatus.CONSTELLATION_GALILEO:
                codeL1 = Constants.CODE_L1X;
                codeL5 = Constants.CODE_L5X;
                break;
            case GnssStatus.CONSTELLATION_BEIDOU:
                codeL1 = Constants.CODE_L1I;
                codeL5 = Constants.CODE_L6I;
                break;
            case GnssStatus.CONSTELLATION_GLONASS:
                codeL1 = Constants.CODE_L1C;
                codeL5 = Constants.CODE_L5X;
                break;
            default:
                Log.e("COMP", "Unknown constellation.");
                return null;
        }

        // Selecting the right code for the measurement
        if(processingOptions.isIonofreeEnabled()
                && satelliteObservation.isL3Enabled())
        {
            int codeL3 = 0; // No DCB if iono-free (in theory...)

            meas = new EnhancedLSE.Measurements(
                    satelliteObservation.getPseudorangeL3(),
                    satelliteObservation.getCn0L1(),
                    codeL3);

            measurementsList.add(meas);
        }
        else if(processingOptions.isDualFrequencyEnabled())
        {
            if(satelliteObservation.isL1Enabled())
            {
                meas = new EnhancedLSE.Measurements(
                        satelliteObservation.getPseudorangeL1(),
                        satelliteObservation.getCn0L1(),
                        codeL1);

                measurementsList.add(meas);
            }

            if(satelliteObservation.isL5Enabled())
            {
                meas = new EnhancedLSE.Measurements(
                        satelliteObservation.getPseudorangeL5(),
                        satelliteObservation.getCn0L5(),
                        codeL5);

                measurementsList.add(meas);
            }
        }
        else if(processingOptions.isMonoFrequencyEnabled())
        {
            if(satelliteObservation.isL1Enabled())
            {
                meas = new EnhancedLSE.Measurements(
                        satelliteObservation.getPseudorangeL1(),
                        satelliteObservation.getCn0L1(),
                        codeL1);

                measurementsList.add(meas);
            }
        }

        return measurementsList;
    }

    //----------------------------------------------------------------------------------------------

    // Function to get co-ordinates
    public Coordinates getPosition()
    {
        return position;
    }

    //----------------------------------------------------------------------------------------------

    // Function to set current systems
    private void setCurrentSystems()
    {
        hashMapSystemIndex = new HashMap<>();
        int k = 0;

        for (HashMap.Entry<String, GNSSObservation> entry : MapSatelliteObservations.entrySet())
        {
            //Reject Beidou Measurements
            /*if (entry.getValue().getConstellation() == 5)
            {
                continue;
            }*/

            if(hashMapSystemIndex.isEmpty())
            {
                hashMapSystemIndex.put(entry.getValue().getConstellation(), IDX_C_DTR);
            }
            else if(!hashMapSystemIndex.keySet().contains(entry.getValue().getConstellation()))
            {
                hashMapSystemIndex.put(entry.getValue().getConstellation(), MINIMAL_PARAM + k);

                k += 1;
            }
        }
    }

    //----------------------------------------------------------------------------------------------

    // Function to get number of observations
    public int getNumberObservations()
    {
        int nbObs = 0;
        for(Map.Entry<String, GNSSObservation> entry : MapSatelliteObservations.entrySet())
        {
            satelliteObservation = entry.getValue();

            if(processingOptions.isIonofreeEnabled())
            {
                if(satelliteObservation.getPseudorangeL3() > 0.0)
                {
                    nbObs ++;
                }
            }
            else if(processingOptions.isDualFrequencyEnabled())
            {
                if(satelliteObservation.getPseudorangeL1() > 0.0)
                {
                    nbObs ++;
                }
                if(satelliteObservation.getPseudorangeL5() > 0.0)
                {
                    nbObs ++;
                }
            }
            else if(processingOptions.isMonoFrequencyEnabled())
            {
                if(satelliteObservation.getPseudorangeL1() > 0.0)
                {
                    nbObs ++;
                }
            }
        }

        return nbObs;
    }

    //----------------------------------------------------------------------------------------------

    // Function to get measurement weight
    private double getWeightValue(double cn0)
    {
        double sigma = 0.0;

        double a = 10;
        double b = Math.pow(150, 2);

        sigma = (a + b * Math.pow(10, (-0.1 * cn0)));

        return sigma;
    }

    // Measurement class for easier handling
    class Measurements
    {
        double pseudo;
        double cn0;
        int codeFreq;

        public Measurements(double pseudo, double cn0, int codeFreq)
        {
            this.pseudo = pseudo;
            this.cn0 = cn0;
            this.codeFreq = codeFreq;
        }
    }

    //----------------------------------------------------------------------------------------------

    public SimpleMatrix getX()
    {
        return X;
    }

    public double getGdop() {
        return 0.0;
    }

}
