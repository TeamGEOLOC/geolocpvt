package fr.ifsttar.geoloc.geolocpvt.fragments;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import org.ejml.simple.SimpleMatrix;

import java.text.DecimalFormat;
import java.util.Objects;

import fr.ifsttar.geoloc.geoloclib.Coordinates;
import fr.ifsttar.geoloc.geoloclib.Utils;
import fr.ifsttar.geoloc.geolocpvt.R;

public class ReferenceFragment extends Fragment implements View.OnFocusChangeListener {

    private EditText referenceXET;
    private EditText referenceYET;
    private EditText referenceZET;
    private EditText referenceLatET;
    private EditText referenceLonET;
    private EditText referenceHeightET;

    private TextView eastTV;
    private TextView northTV;
    private TextView upTV;

    private Coordinates refCoord;

    //defining the xml for the fragment
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_reference, null);

        referenceXET = view.findViewById(R.id.referenceXET);
        referenceYET = view.findViewById(R.id.referenceYET);
        referenceZET = view.findViewById(R.id.referenceZET);
        referenceLatET = view.findViewById(R.id.referenceLatET);
        referenceLonET = view.findViewById(R.id.referenceLonET);
        referenceHeightET = view.findViewById(R.id.referenceHeightET);

        eastTV = view.findViewById(R.id.eastTV);
        northTV = view.findViewById(R.id.northTV);
        upTV = view.findViewById(R.id.upTV);

        refCoord = new Coordinates(
                Double.parseDouble(referenceXET.getText().toString()),
                Double.parseDouble(referenceYET.getText().toString()),
                Double.parseDouble(referenceZET.getText().toString()));

        return view;
    }

    //setting fragment view
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        refreshData();
    }

    public void refreshData(){
        Objects.requireNonNull(getActivity()).runOnUiThread(new Runnable() {
            @Override
            public void run()
            {
                Bundle bundle;

                if (getArguments() != null) {
                    bundle = getArguments();

                    Coordinates userCoordinates = (Coordinates) bundle.getSerializable("ComputedPosition");

                    DecimalFormat cartesian = new DecimalFormat("#.###");

                    if (userCoordinates != null && refCoord != null)
                    {
                        SimpleMatrix enu = Utils.getENU(refCoord, userCoordinates);

                        eastTV.setText(cartesian.format(enu.get(0,0)));
                        northTV.setText(cartesian.format(enu.get(1,0)));
                        upTV.setText(cartesian.format(enu.get(2,0)));
                    }
                }
            }
        });
    }

    @Override
    public void onFocusChange(View view, boolean hasFocus)
    {
        switch (view.getId())
        {
            case R.id.referenceXET:
            case R.id.referenceYET:
            case R.id.referenceZET:
                refCoord = new Coordinates(
                        Double.parseDouble(referenceXET.getText().toString()),
                        Double.parseDouble(referenceYET.getText().toString()),
                        Double.parseDouble(referenceZET.getText().toString()));
                break;
            case R.id.referenceLatET:
            case R.id.referenceLonET:
            case R.id.referenceHeightET:
                refCoord = new Coordinates();
                refCoord.setFromGeographics(
                        Double.parseDouble(referenceLatET.getText().toString()),
                        Double.parseDouble(referenceLonET.getText().toString()),
                        Double.parseDouble(referenceHeightET.getText().toString()));
                break;
        }

        refreshReference();
    }

    public Coordinates getReference()
    {
        return refCoord;
    }

    public void refreshReference(){
        if(getActivity() == null)
        {
            return;
        }
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run()
            {
                DecimalFormat cartesian = new DecimalFormat("#.###");
                DecimalFormat geographic = new DecimalFormat("#.########");

                referenceXET.setText(cartesian.format(refCoord.getX()));
                referenceYET.setText(cartesian.format(refCoord.getY()));
                referenceZET.setText(cartesian.format(refCoord.getZ()));
                referenceLatET.setText(geographic.format(refCoord.getLatLngAlt().getLatitude()));
                referenceLonET.setText(geographic.format(refCoord.getLatLngAlt().getLongitude()));
                referenceHeightET.setText(geographic.format(refCoord.getLatLngAlt().getAltitude()));
            }
        });
    }
}