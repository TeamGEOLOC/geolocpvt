///=================================================================================================
// Class MainActivity
//      Author :  Jose Gilberto RESENDIZ FONSECA
//        Date :  2019/09/06
///=================================================================================================
/*
 * Copyright 2018(c) IFSTTAR - TeamGEOLOC
 *
 * This file is part of the GeolocPVT application.
 *
 * GeolocPVT is distributed as a free software in order to build a community of users, contributors,
 * developers who will contribute to the project and ensure the necessary means for its evolution.
 *
 * GeolocPVT is a free software; you can redistribute it and/or modify it under the terms of the
 * GNU Lesser General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version. Any modification of source code in this
 * LGPL software must also be published under the LGPL license.
 *
 * GeolocPVT is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the GNU Lesser General Public License along with GeolocPVT.
 * If not, see <https://www.gnu.org/licenses/lgpl.txt/>.
 */
///=================================================================================================
package fr.ifsttar.geoloc.geolocpvt.fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.Toast;

import fr.ifsttar.geoloc.geolocpvt.R;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.util.Objects;

import static android.content.ContentValues.TAG;

/**
 * Class LoggerFragment
 */
public class LoggerFragment extends Fragment {

    /**
     * to manipulate graphic items
     */
    private ListView mList;
    private ArrayAdapter<String> mArrayAdapter;

    private Button btnOpen;
    private Button btnDelete;
    private Button btnShare;  //TODO share button
    private Button btnRefresh;
    private CheckBox checkPseudo;
    private CheckBox checkEphem;
    private CheckBox checkPos;

    String path = Environment.getExternalStorageDirectory().getAbsolutePath() + "//IFSTTAR_GNSS_logger//";



    private String[] mFileList;
    private File mPath = new File(path);
    private String mChosenFile;
    private static final String FTYPE = ".txt";
    private static final int DIALOG_LOAD_FILE = 1000;

    /**
     * define the xml for the fragment
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_logger, null);
    }

    /**
     * set fragment view
     *
     * @param view
     * @param savedInstanceState
     */
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        //we initialize the objects associated with the graphic items
        mList = view.findViewById(R.id.listFiles);

        mList.setOnItemLongClickListener((parent, view1, position, id) -> {
            String fileName = mArrayAdapter.getItem(position);
            //File f = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/IFSTTAR_GNSS_logger/" + fileName);
            File f = new File(Environment.getExternalStorageDirectory() + "/IFSTTAR_GNSS_logger/" + fileName);


            try {
                BufferedReader br = new BufferedReader(new FileReader(f));
                StringBuilder content = new StringBuilder();
                String line = null;
                while ((line = br.readLine()) != null) {
                    content.append(line);
                }
                br.close();
                Toast.makeText(getContext(), content.toString(), Toast.LENGTH_SHORT).show();
                //TODO changer l'affichage et mettre un DialogFragment
            } catch (Exception e) {
                e.printStackTrace();
            }

            return true;
        });


        btnOpen = view.findViewById(R.id.buttonOpen);
        //      btnOpen.setOnClickListener((View.OnClickListener) this);
        btnDelete = view.findViewById(R.id.buttonDelete);
//        btnDelete.setOnClickListener((View.OnClickListener) this);
        btnRefresh = view.findViewById(R.id.buttonRefresh);
        btnRefresh.setEnabled(false);

        checkPseudo = view.findViewById(R.id.checkBoxPseudorange);
        checkEphem = view.findViewById(R.id.checkBoxEphemeris);
        checkPos = view.findViewById(R.id.checkBoxPosition);

        refreshData();
    }

    /*
     @Override
    public void onClick(View v) {

        switch (v.getId())
        // action for the button pressed by the user
        {
            case R.id.buttonRefresh:
                refreshData();
                break;

            case R.id.buttonOpen:
                btnOpen.setEnabled(true);
                break;

        }
    }
*/


    /**
     * refresh the data
     */
    public void refreshData() {
        Objects.requireNonNull(getActivity()).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                //TODO show files to user
                loadFileList();
                /*
                 if(!checkPos.isChecked() && !checkPseudo.isChecked() && !checkEphem.isChecked()) {
                        Toast.makeText(getApplicationContext(),"You have to check at least one checkbox",Toast.LENGTH_SHORT).show();
                    }
                 else if (checkPos.isChecked() && checkPseudo.isChecked() && checkEphem.isChecked()) {
                 //tous les fichiers



/*
protected Dialog onCreateDialog(int id) {
    Dialog dialog = null;
    AlertDialog.Builder builder = new Builder(this);

    switch(id) {
        case DIALOG_LOAD_FILE:
            builder.setTitle("Choose your file");
            if(mFileList == null) {
                Log.e(TAG, "Showing file picker before loading the file list");
                dialog = builder.create();
                return dialog;
            }
            builder.setItems(mFileList, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    mChosenFile = mFileList[which];
                    //you can do stuff with the file here too
                }
            });
            break;
    }
    dialog = builder.show();
    return dialog;
} */
/*
                 }
                 else if (checkPos.isChecked() && checkPseudo.isChecked() && !checkEphem.isChecked()) {
                 // TODO

                 }
                 else if (checkPos.isChecked() && !checkPseudo.isChecked() && checkEphem.isChecked()) {
                 // TODO

                 }
                 else if (!checkPos.isChecked() && checkPseudo.isChecked() && checkEphem.isChecked()) {
                 // TODO

                 }

                    //from each satellite, GnssClock is the same for all satellites
                    List<SatellitePseudorange> mSatellites = new ArrayList<SatellitePseudorange>();
                    //creating an adapter for our list
                    ArrayAdapter<SatellitePseudorange> mArrayAdapter = new ArrayAdapter<SatellitePseudorange>(
                            getActivity().getApplicationContext(),
                            android.R.layout.simple_list_item_1,
                            mSatellites);

                    mList.setAdapter(mArrayAdapter);
                 */


            }
        });
    }

    private void loadFileList() {
        try {
            mPath.mkdirs();
        } catch (SecurityException e) {
            Log.e(TAG, "unable to write on the sd card " + e.toString());
        }
        if (mPath.exists()) {
            FilenameFilter filter = new FilenameFilter() {

                @Override
                public boolean accept(File dir, String filename) {
                    File sel = new File(dir, filename);
                    return filename.contains(FTYPE) || sel.isDirectory();
                }

            };
            mFileList = mPath.list(filter);
        } else {
            mFileList = new String[0];
        }
        mArrayAdapter = new ArrayAdapter<String>(
                Objects.requireNonNull(getActivity()).getApplicationContext(),
                android.R.layout.simple_list_item_1,
                mFileList);

        mList.setAdapter(mArrayAdapter);
    }


}
