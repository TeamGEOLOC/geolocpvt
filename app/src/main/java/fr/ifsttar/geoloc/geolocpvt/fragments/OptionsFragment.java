///=================================================================================================
// Class OptionsFragment
//      Author :  Antoine GRENIER - 2019/09/06
//        Date :  2019/09/06
///=================================================================================================
/*
 * Copyright 2018(c) IFSTTAR - TeamGEOLOC
 *
 * This file is part of the GeolocPVT application.
 *
 * GeolocPVT is distributed as a free software in order to build a community of users, contributors,
 * developers who will contribute to the project and ensure the necessary means for its evolution.
 *
 * GeolocPVT is a free software; you can redistribute it and/or modify it under the terms of the
 * GNU Lesser General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version. Any modification of source code in this
 * LGPL software must also be published under the LGPL license.
 *
 * GeolocPVT is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the GNU Lesser General Public License along with GeolocPVT.
 * If not, see <https://www.gnu.org/licenses/lgpl.txt/>.
 */
///=================================================================================================
package fr.ifsttar.geoloc.geolocpvt.fragments;

import android.location.GnssStatus;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.TextView;

import java.util.Objects;
import java.util.Vector;

import fr.ifsttar.geoloc.geolocpvt.R;
import fr.ifsttar.geoloc.geoloclib.Options;

public class OptionsFragment extends Fragment {

    private Options processingOptions;

    private CheckBox gpsCB;
    private CheckBox galCB;
    private CheckBox bdsCB;
    private CheckBox gloCB;
    private RadioButton streamsRB;
    private CheckBox streamsCorrCB;
    private RadioButton pppRB;
    private RadioButton monoFreqRB;
    private RadioButton dualFreqRB;
    private RadioButton ionoFreeRB;

    private RadioButton staticModeRB;
    private RadioButton dynamicModeRB;

    private RadioGroup processingRG;
    private RadioGroup frequenciesRG;

    private CheckBox smoothingCB;
    private CheckBox kalmanCB;
    private CheckBox dcbCB;
    private CheckBox ionoCorrCB;

    private SeekBar elevSB;
    private SeekBar noiseSB;

    // Constants

    private final static int COEFFICIENT_SEEKBAR = 5;

    //----------------------------------------------------------------------------------------------

    //defining the xml for the fragment
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_options, null);
        processingOptions = new Options();

        gpsCB = view.findViewById(R.id.cb_gps);
        galCB = view.findViewById(R.id.cb_gal);
        bdsCB = view.findViewById(R.id.cb_bds);
        gloCB = view.findViewById(R.id.cb_glo);
        streamsRB = view.findViewById(R.id.rb_streams);
        streamsCorrCB = view.findViewById(R.id.cb_rtcmCorr);
        pppRB = view.findViewById(R.id.rb_ppp);
        monoFreqRB = view.findViewById(R.id.rb_l1);
        dualFreqRB = view.findViewById(R.id.rb_l1l2);
        ionoFreeRB = view.findViewById(R.id.rd_l3);

        kalmanCB = view.findViewById(R.id.cb_kalman);
        ionoCorrCB = view.findViewById(R.id.checkBox8);

        processingRG = view.findViewById(R.id.radioGroupProcessing);
        frequenciesRG = view.findViewById(R.id.radioGroupFrequencies);

        smoothingCB = view.findViewById(R.id.cb_smoothing);

        dcbCB = view.findViewById(R.id.cb_dcb);

        staticModeRB = view.findViewById(R.id.rb_static);
        dynamicModeRB = view.findViewById(R.id.rb_dynamic);

        elevSB = view.findViewById(R.id.elevCutoffSB);
        TextView elevTV = view.findViewById(R.id.elevCutoffTV);
        elevTV.setText(String.format("%d °", elevSB.getProgress() * COEFFICIENT_SEEKBAR));
        elevSB.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                elevTV.setText(String.format("%d °", elevSB.getProgress() * COEFFICIENT_SEEKBAR));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        noiseSB = view.findViewById(R.id.noiseCutoffSB);
        TextView noiseTV = view.findViewById(R.id.noiseCutoffTV);
        noiseTV.setText(String.format("%d dbHz", noiseSB.getProgress() * COEFFICIENT_SEEKBAR));
        noiseSB.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                noiseTV.setText(String.format("%d dbHz", noiseSB.getProgress() * COEFFICIENT_SEEKBAR));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        return view;

    }

    //----------------------------------------------------------------------------------------------

    //setting fragment view
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        refreshData();
    }

    public void refreshData(){
        if (getActivity() == null)
        {
            return;
        }
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                refreshProcessingOptions();
                //TODO REPLACE THIS FUNCTION BY A EVENT CHANGE LISTENER
            }
        });
    }

    //----------------------------------------------------------------------------------------------

    private void refreshProcessingOptions()
    {
        // Check ticked systems*
        //TODO Change Vector to Set
        Vector<Integer> systemsEnabled = new Vector<>();
        if(gpsCB.isChecked())
        {
            systemsEnabled.add(GnssStatus.CONSTELLATION_GPS);
        }
        else
        {
            if(systemsEnabled.contains(GnssStatus.CONSTELLATION_GPS))
            {
                systemsEnabled.removeElement(GnssStatus.CONSTELLATION_GPS);
            }
        }
        if(galCB.isChecked())
        {
            systemsEnabled.add(GnssStatus.CONSTELLATION_GALILEO);
        }
        else {
            if(systemsEnabled.contains(GnssStatus.CONSTELLATION_GALILEO))
            {
                systemsEnabled.removeElement(GnssStatus.CONSTELLATION_GALILEO);
            }
        }
        if(bdsCB.isChecked())
        {
            systemsEnabled.add(GnssStatus.CONSTELLATION_BEIDOU);
        }
        else {
            if(systemsEnabled.contains(GnssStatus.CONSTELLATION_BEIDOU))
            {
                systemsEnabled.removeElement(GnssStatus.CONSTELLATION_BEIDOU);
            }
        }
        /*if(gloCB.isChecked())
        {
            systemsEnabled.add(GnssStatus.CONSTELLATION_GLONASS);
        }
        else {
            if(systemsEnabled.contains(GnssStatus.CONSTELLATION_GLONASS))
            {
                systemsEnabled.removeElement(GnssStatus.CONSTELLATION_GLONASS);
            }
        }*/

        gloCB.setEnabled(false);
        processingOptions.setSystemsEnabled(systemsEnabled);

        streamsRB.setEnabled(false);
        streamsCorrCB.setEnabled(false);

        //TODO Add other corrections

        if(monoFreqRB.isChecked())
        {
            processingOptions.setMonoFrequencyEnabled(true);
            processingOptions.setDualFrequencyEnabled(false);
            processingOptions.setIonofreeEnabled(false);
        }
        else if (dualFreqRB.isChecked())
        {
            processingOptions.setDualFrequencyEnabled(true);
            processingOptions.setMonoFrequencyEnabled(false);
            processingOptions.setIonofreeEnabled(false);
        }
        else if (ionoFreeRB.isChecked())
        {
            processingOptions.setIonofreeEnabled(true);
            processingOptions.setMonoFrequencyEnabled(false);
            processingOptions.setDualFrequencyEnabled(false);
        }

        pppRB.setEnabled(false);
        switch (processingRG.getCheckedRadioButtonId())
        {
            case R.id.rb_ppp:
                processingOptions.setPppEnabled(true);
                processingOptions.setSppEnabled(false);
                break;
            case R.id.rb_spp:
                processingOptions.setSppEnabled(true);
                processingOptions.setPppEnabled(false);
        }

        /*if(smoothingCB.isChecked())
        {
            processingOptions.setSmoothingEnabled(true);
        }
        else {
            processingOptions.setSmoothingEnabled(false);
        }*/
        smoothingCB.setEnabled(false);

        /*if(kalmanCB.isChecked())
        {
            processingOptions.setKalmanEnabled(true);
        }
        else
        {
            processingOptions.setKalmanEnabled(false);
        }*/
        kalmanCB.setEnabled(false);

        if(ionoCorrCB.isChecked())
        {
            processingOptions.setIonoCorrEnabled(true);
        }
        else
        {
            processingOptions.setIonoCorrEnabled(false);
        }

        // Un-comment the below code block to use DCB
        /*if(dcbCB.isChecked())
        {
            processingOptions.setDcbEnabled(true);
        }
        else
        {
            processingOptions.setDcbEnabled(false);
        }*/

        // Differential Code Bias Disabled, comment below line and un-comment the above segment to use DCB
        dcbCB.setEnabled(false);

        //Dynamic mode

        if(staticModeRB.isChecked())
        {
            processingOptions.setDynamicMode(false);
            //smoothingCB.setEnabled(true);
            //kalmanCB.setEnabled(true);
        }
        else if(dynamicModeRB.isChecked())
        {
            processingOptions.setDynamicMode(true);
            //smoothingCB.setChecked(true);
            //smoothingCB.setEnabled(false);
            //kalmanCB.setChecked(true);
            //kalmanCB.setEnabled(false);
        }

        // Cutoffs
        processingOptions.setCutoffAngle(elevSB.getProgress() * COEFFICIENT_SEEKBAR);
        processingOptions.setCutoffNoise(noiseSB.getProgress() * COEFFICIENT_SEEKBAR);

    }

    //----------------------------------------------------------------------------------------------

    public Options getProcessingOptions()
    {
        return processingOptions;
    }

    //----------------------------------------------------------------------------------------------

    public void disbView()
    {
        gpsCB.setEnabled(false);
        galCB.setEnabled(false);
        bdsCB.setEnabled(false);
        gloCB.setEnabled(false);
        streamsRB.setEnabled(false);
        streamsCorrCB.setEnabled(false);
        //pppRB.setEnabled(false);
        monoFreqRB.setEnabled(false);
        dualFreqRB.setEnabled(false);
        ionoFreeRB.setEnabled(false);
        staticModeRB.setEnabled(false);
        dynamicModeRB.setEnabled(false);
        //smoothingCB.setEnabled(false);
        //kalmanCB.setEnabled(false);
        ionoCorrCB.setEnabled(false);
        elevSB.setEnabled(false);
        noiseSB.setEnabled(false);
    }

    //----------------------------------------------------------------------------------------------

    public void enbView()
    {
        gpsCB.setEnabled(true);
        galCB.setEnabled(true);
        bdsCB.setEnabled(true);
        gloCB.setEnabled(true);
        streamsRB.setEnabled(true);
        streamsCorrCB.setEnabled(true);
        //pppRB.setEnabled(true);
        monoFreqRB.setEnabled(true);
        dualFreqRB.setEnabled(true);
        ionoFreeRB.setEnabled(true);
        staticModeRB.setEnabled(true);
        dynamicModeRB.setEnabled(true);
        //smoothingCB.setEnabled(true);
        //kalmanCB.setEnabled(true);
        ionoCorrCB.setEnabled(true);
        elevSB.setEnabled(true);
        noiseSB.setEnabled(true);
    }

}