package fr.ifsttar.geoloc.geolocpvt.fragments;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import fr.ifsttar.geoloc.geolocpvt.R;

public class ImuFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_imu, null);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    public void refreshData() {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                //creating a bundle to receive data from main activity
                Bundle bundle = new Bundle();

                if(getArguments() != null) {
                    //then we recuperate arguments in our bundle object
                    bundle = getArguments();
                }

                String str = (String)getArguments().get("str");


            }
        });
    }
}
