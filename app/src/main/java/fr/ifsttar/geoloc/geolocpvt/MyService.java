package fr.ifsttar.geoloc.geolocpvt;

import android.app.Notification;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.app.PendingIntent;
import android.os.IBinder;
import android.widget.Toast;
import android.os.PowerManager;
import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

import static fr.ifsttar.geoloc.geolocpvt.MainActivity.CHANNEL_ID;

public class MyService extends Service {
    public MyService() { }

    private PowerManager pm;
    private PowerManager.WakeLock wl;

    @Override
    public void onCreate() {
        Toast.makeText(this, "Service Created", Toast.LENGTH_LONG).show();
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Toast.makeText(this, "Service Started", Toast.LENGTH_LONG).show();

        pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        wl = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,  "MyApp::MyWakelockTag");
        wl.acquire(10*60*1000L /*10 minutes*/);

        Intent notificationIntent = new Intent(this, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this,
                0, notificationIntent, 0);

        Notification notify = new NotificationCompat.Builder(this,CHANNEL_ID)
                .setContentTitle("Running...")
                .setContentText("StandBy...")
                .setSmallIcon(R.drawable.red_marker)
                .setContentIntent(pendingIntent)
                .build();
        startForeground(1,notify);

        return START_NOT_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) { return null;}

    @Override
    public void onDestroy() {
        Toast.makeText(this, "Service Stopped", Toast.LENGTH_LONG).show();
        super.onDestroy();
    }

}
