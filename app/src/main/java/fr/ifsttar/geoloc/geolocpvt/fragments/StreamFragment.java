///=================================================================================================
// Class StreamFragment
//      Author :  Antoine GRENIER - 2019/09/09
//        Date :  2019/09/09
///=================================================================================================
/*
 * Copyright 2018(c) IFSTTAR - TeamGEOLOC
 *
 * This file is part of the GeolocPVT application.
 *
 * GeolocPVT is distributed as a free software in order to build a community of users, contributors,
 * developers who will contribute to the project and ensure the necessary means for its evolution.
 *
 * GeolocPVT is a free software; you can redistribute it and/or modify it under the terms of the
 * GNU Lesser General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version. Any modification of source code in this
 * LGPL software must also be published under the LGPL license.
 *
 * GeolocPVT is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the GNU Lesser General Public License along with GeolocPVT.
 * If not, see <https://www.gnu.org/licenses/lgpl.txt/>.
 */
///=================================================================================================
package fr.ifsttar.geoloc.geolocpvt.fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;
import java.util.SortedSet;
import java.util.TreeSet;

import fr.ifsttar.geoloc.geoloclib.streams.ConnectionParameters;
import fr.ifsttar.geoloc.geolocpvt.R;

public class StreamFragment extends Fragment
{
    private ListView mountpointsLV;

    private ConnectionParameters connectionParameters = new ConnectionParameters();

    private TextView connectionStatusTV;

    private EditText hostET;
    private EditText portET;
    private EditText userIdET;
    private TextInputEditText passET;

    private EditText addMountpointET;

    private HashMap<String, Boolean> mountpointsMap;

    boolean isPasswordValid, isIdValid, isMountpointNameValid;


    //defining the xml for the fragment
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_streams, null);

        mountpointsLV = view.findViewById(R.id.listMountpointsLV);

        hostET = view.findViewById(R.id.hostET);
        portET = view.findViewById(R.id.portET);
        userIdET = view.findViewById(R.id.userIdET);
        passET = view.findViewById(R.id.passwordET);

        addMountpointET = view.findViewById(R.id.addedMountET);

        connectionStatusTV = view.findViewById(R.id.connectionStatusTV);


        Button connection = view.findViewById(R.id.connectButton);

        connection.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                // Récupération du contenu de l'EditText
                refreshStreamOptions();
                refreshData();
                SetValidation();
                Log.i("Click Connection", "Connection button is working");
            }

            private void SetValidation() {

                // Check for a valid password.
                if (passET.getText().toString().isEmpty()) {
                    passET.setError("Enter a PassWord");
                    isPasswordValid = false;
                } else if (!passET.getText().toString().equals("centipede")) { // || !passET.getText().toString().equals("Infinity18")
                    passET.setError("Enter a valid password");
                    isPasswordValid = false;
                } else  {
                    isPasswordValid = true;
                }

                // Check for a valid Id.
                if (userIdET.getText().toString().isEmpty()) {
                    userIdET.setError("Enter a User Id");
                    isIdValid = false;
                } else if (!userIdET.getText().toString().equals("centipede")) {//|| !userIdET.getText().toString().equals("InfiniteESA")
                    userIdET.setError("Enter a valid User Id");
                    isIdValid = false;
                } else  {
                    isPasswordValid = true;
                }

                if (isIdValid && isPasswordValid) {
                    Toast.makeText(getContext(), "Successfully", Toast.LENGTH_SHORT).show();
                }
            }
        });

        Button addMountpointButton = view.findViewById(R.id.addMountButton);

        addMountpointButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mountpointsMap.put(addMountpointET.getText().toString(), false);
                SetMountPointValidation();
            }

            private void SetMountPointValidation() {
                // Check for a valid Mount Point Name.
                if (addMountpointET.getText().toString().isEmpty()) {
                    addMountpointET.setError("Enter a Mount Point Name");
                    isMountpointNameValid = false;
                } else  {
                    isMountpointNameValid = true;
                }
            }
        });

        mountpointsLV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                AlertDialog.Builder builder1 = new AlertDialog.Builder(getActivity());
                builder1.setMessage("Do you want to delete this mount point ?");
                builder1.setCancelable(true);

                builder1.setPositiveButton(
                        "Yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                String key = "";
                                for (HashMap.Entry<String, Boolean> entry : mountpointsMap.entrySet())
                                {
                                    if(parent.getItemAtPosition(position).toString().contains(entry.getKey()))
                                    {
                                        key = entry.getKey();
                                    }
                                }
                                mountpointsMap.remove(key);

                                dialog.cancel();
                            }
                        });

                builder1.setNegativeButton(
                        "No",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog alert11 = builder1.create();
                alert11.show();

            }
        });

        addMountpointET.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {
                    addMountpointET.setText("");
                }
            }
        });

        return view;
    }

    //setting fragment view
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        refreshData();
    }

    /**
     * used for refreshing to current data
     */
    public void refreshData(){
        if(getActivity() == null)
        {
            return;
        }
        getActivity().runOnUiThread(() -> {
            //creating a bundle to receive data from main activity
            Bundle bundle = new Bundle();

            if(getArguments() != null) {
                //then we recuperate arguments in our bundle object
                bundle = getArguments();
            }

            mountpointsMap = (HashMap<String, Boolean>) bundle.getSerializable("Mountpoints");

            if(mountpointsMap != null)
            {
                SortedSet<String> mountpointsArray = new TreeSet<>();

                for (HashMap.Entry<String, Boolean> entry : mountpointsMap.entrySet()) {

                    String strConnection;


                    if(entry.getValue())
                    {
                        strConnection = "Connected";

                        connectionStatusTV.setText("Connected");
                        connectionStatusTV.setTextColor(Color.GREEN);
                    }

                    else
                    {
                        connectionStatusTV.setText("Not connected");
                        connectionStatusTV.setTextColor(Color.RED);
                        strConnection = "Not connected";
                    }

                    mountpointsArray.add(String.format("%-10s -> %-15s", entry.getKey(), strConnection));
                }

                ArrayAdapter<String> mArrayAdapter = new ArrayAdapter<>(
                        getActivity().getApplicationContext(),
                        R.layout.custom_item_list,
                        R.id.tvList,
                        new ArrayList<>(mountpointsArray));

                mountpointsLV.setAdapter(mArrayAdapter);
            }
        });


    }

    //----------------------------------------------------------------------------------------------

    public void refreshStreamOptions()
    {
        Log.i("TestDebug" ,"refreshstreamoptions In");
        Log.i("TestDebugHost" ,"host "+ hostET.getText().toString());
        Log.i("TestDebug" ,"port "+ portET.getText().toString());
        Log.i("TestDebug" ,"user "+ userIdET.getText().toString());
        Log.i("TestDebug" ,"pass "+ passET.getText().toString());
        connectionParameters.setHost(hostET.getText().toString());
        connectionParameters.setPort(Integer.parseInt(portET.getText().toString()));
        connectionParameters.setUser(userIdET.getText().toString());
        connectionParameters.setPassword(passET.getText().toString());

        Log.i("TestDebug" ,"refreshstreamoptions Out");
    }

    //----------------------------------------------------------------------------------------------

    public HashMap<String, Boolean> getMountpointsMap() {
        return mountpointsMap;
    }

    public ConnectionParameters getConnectionParameters() {
        return connectionParameters;
    }

    //----------------------------------------------------------------------------------------------

    public void setMountpointsMap(HashMap<String, Boolean> mountpointsMap)
    {
        this.mountpointsMap = mountpointsMap;
    }

}
