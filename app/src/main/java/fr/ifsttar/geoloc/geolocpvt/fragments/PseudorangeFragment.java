///=================================================================================================
// Class MainActivity
//      Author :  Jose Gilberto RESENDIZ FONSECA
// Modified by :  Antoine GRENIER - 2019/09/06
//        Date :  2019/09/06
///=================================================================================================
/*
 * Copyright 2018(c) IFSTTAR - TeamGEOLOC
 *
 * This file is part of the GeolocPVT application.
 *
 * GeolocPVT is distributed as a free software in order to build a community of users, contributors,
 * developers who will contribute to the project and ensure the necessary means for its evolution.
 *
 * GeolocPVT is a free software; you can redistribute it and/or modify it under the terms of the
 * GNU Lesser General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version. Any modification of source code in this
 * LGPL software must also be published under the LGPL license.
 *
 * GeolocPVT is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the GNU Lesser General Public License along with GeolocPVT.
 * If not, see <https://www.gnu.org/licenses/lgpl.txt/>.
 */
///=================================================================================================
package fr.ifsttar.geoloc.geolocpvt.fragments;

import android.location.GnssStatus;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.appcompat.widget.AppCompatSpinner;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.RadioGroup;

import org.gogpsproject.ephemeris.GNSSEphemeris;

import fr.ifsttar.geoloc.geolocpvt.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;
import java.util.SortedSet;
import java.util.TreeSet;

/**
 * the fragment where to see the satellite and their pseudoranges
 */
public class PseudorangeFragment extends Fragment implements RadioGroup.OnCheckedChangeListener{
    /**
     * mList : to store the data of each visible satellite
     */
    private ListView mList;
    /**
     * RadioGroup: to choose the constellation
     */
    private RadioGroup radioGroup;
    private int constellation;

    AppCompatSpinner mySpinner;

    //defining the xml for the fragment
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return  inflater.inflate(R.layout.fragment_pseudorange, null);
    }

    //setting fragment view
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        //checking that arguments are not null
        //creating list to show satellites and pseudoranges in our view
        mList = view.findViewById(R.id.list);

        radioGroup = view.findViewById(R.id.radioGroupConstellations);
        radioGroup.setOnCheckedChangeListener(this);
        refreshData();
    }


    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch (checkedId){
            case R.id.radioButtonGps:
                constellation = GnssStatus.CONSTELLATION_GPS;
                break;
            case R.id.radioButtonBeidou:
                constellation = GnssStatus.CONSTELLATION_BEIDOU;
                break;
            case R.id.radioButtonGal:
                constellation = GnssStatus.CONSTELLATION_GALILEO;

                break;
        }
    }

    /**
     * used for refreshing to current data
     */
    public void refreshData(){
        Objects.requireNonNull(getActivity()).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                //creating a bundle to receive data from main activity
                Bundle bundle = new Bundle();

                if(getArguments() != null) {
                    //then we recuperate arguments in our bundle object
                    bundle = getArguments();
                }

                HashMap< String, GNSSEphemeris> eph = (HashMap<String, GNSSEphemeris>) bundle.getSerializable("SatelliteEphemeris");

                if(bundle.getSerializable("SatelliteEphemeris") != null) {
                    SortedSet<String> mSatellites = new TreeSet<>();

                    for (HashMap.Entry<String, GNSSEphemeris> entry : eph.entrySet()) {
                        if (entry.getValue().getGnssSystem() == constellation) {
                            mSatellites.add(entry.getKey());
                        }
                    }

                    ArrayAdapter<String> mArrayAdapter = new ArrayAdapter<String>(
                            getActivity().getApplicationContext(),
                            R.layout.custom_item_list,
                            R.id.tvList,
                            new ArrayList<String>(mSatellites));

                    mList.setAdapter(mArrayAdapter);
                }
            }
        });
    }

}
