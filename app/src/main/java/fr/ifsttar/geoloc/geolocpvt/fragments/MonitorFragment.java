///=================================================================================================
// Class MainActivity
//      Author :  Jose Gilberto RESENDIZ FONSECA
// Modified by :  Antoine GRENIER - 2019/09/06
//        Date :  2019/09/06
///=================================================================================================
/*
 * Copyright 2018(c) IFSTTAR - TeamGEOLOC
 *
 * This file is part of the GeolocPVT application.
 *
 * GeolocPVT is distributed as a free software in order to build a community of users, contributors,
 * developers who will contribute to the project and ensure the necessary means for its evolution.
 *
 * GeolocPVT is a free software; you can redistribute it and/or modify it under the terms of the
 * GNU Lesser General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version. Any modification of source code in this
 * LGPL software must also be published under the LGPL license.
 *
 * GeolocPVT is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the GNU Lesser General Public License along with GeolocPVT.
 * If not, see <https://www.gnu.org/licenses/lgpl.txt/>.
 */
///=================================================================================================
package fr.ifsttar.geoloc.geolocpvt.fragments;

import android.location.GnssClock;
import android.location.GnssMeasurement;
import android.location.GnssMeasurementsEvent;
import android.location.GnssStatus;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import fr.ifsttar.geoloc.geolocpvt.R;
import fr.ifsttar.geoloc.geoloclib.Coordinates;
import fr.ifsttar.geoloc.geoloclib.satellites.GNSSObservation;
import fr.ifsttar.geoloc.geoloclib.LatLngAlt;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Objects;
import java.util.TimeZone;

/**
 * Class MonitorFragment
 */
public class MonitorFragment extends Fragment {

    private TextView gpsVisL1;
    private TextView gpsVisL5;
    private TextView gpsTrackL1;
    private TextView gpsTrackL5;
    private TextView gpsUsedL1;
    private TextView gpsUsedL5;
    private TextView galVisL1;
    private TextView galVisL5;
    private TextView galTrackL1;
    private TextView galTrackL5;
    private TextView galUsedL1;
    private TextView galUsedL5;
    private TextView bdsVisL1;
    private TextView bdsVisL5;
    private TextView bdsTrackL1;
    private TextView bdsTrackL5;
    private TextView bdsUsedL1;
    private TextView bdsUsedL5;
    private TextView gloVisL1;
    private TextView gloVisL5;
    private TextView gloTrackL1;
    private TextView gloTrackL5;
    private TextView gloUsedL1;
    private TextView gloUsedL5;
    private Coordinates mCoordinates;
    private TextView coordX;
    private TextView coordY;
    private TextView coordZ;
    private TextView coordLat;
    private TextView coordLon;
    private TextView coordAlt;
    private TextView userVelocity;
    private TextView eVel;
    private TextView nVel;
    private TextView uVel;
    private TextView gdopView;
    private TextView timetxt;
    private TextView datetxt;

    /**0
     * define the xml for the fragment
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_monitor, null);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //graphic elements
        gpsVisL1 = view.findViewById(R.id.gpsVisL1);
        gpsVisL5 = view.findViewById(R.id.gpsVisL5);
        gpsTrackL1 = view.findViewById(R.id.gpsTrackL1);
        gpsTrackL5 = view.findViewById(R.id.gpsTrackL5);
        gpsUsedL1 = view.findViewById(R.id.gpsUsedL1);
        gpsUsedL5 = view.findViewById(R.id.gpsUsedL5);

        galVisL1 = view.findViewById(R.id.galVisL1);
        galVisL5 = view.findViewById(R.id.galVisL5);
        galTrackL1 = view.findViewById(R.id.galTrackL1);
        galTrackL5 = view.findViewById(R.id.galTrackL5);
        galUsedL1 = view.findViewById(R.id.galUsedL1);
        galUsedL5 = view.findViewById(R.id.galUsedL5);

        bdsVisL1 = view.findViewById(R.id.bdsVisL1);
        bdsVisL5 = view.findViewById(R.id.bdsVisL5);
        bdsTrackL1 = view.findViewById(R.id.bdsTrackL1);
        bdsTrackL5 = view.findViewById(R.id.bdsTrackL5);
        bdsUsedL1 = view.findViewById(R.id.bdsUsedL1);
        bdsUsedL5 = view.findViewById(R.id.bdsUsedL5);

        gloVisL1 = view.findViewById(R.id.gloVisL1);
        gloVisL5 = view.findViewById(R.id.gloVisL5);
        gloTrackL1 = view.findViewById(R.id.gloTrackL1);
        gloTrackL5 = view.findViewById(R.id.gloTrackL5);
        gloUsedL1 = view.findViewById(R.id.gloUsedL1);
        gloUsedL5 = view.findViewById(R.id.gloUsedL5);

        coordX = view.findViewById(R.id.coordX);
        coordY = view.findViewById(R.id.coordY);
        coordZ = view.findViewById(R.id.coordZ);
        coordLat = view.findViewById(R.id.coordLat);
        coordLon = view.findViewById(R.id.coordLon);
        coordAlt = view.findViewById(R.id.coordAlt);
        userVelocity = view.findViewById(R.id.velTV);
        eVel = view.findViewById(R.id.eVelTV);
        nVel = view.findViewById(R.id.nVelTV);
        uVel = view.findViewById(R.id.uVelTV);

        gdopView = view.findViewById(R.id.gdopTV);
        timetxt = view.findViewById(R.id.time);
        datetxt = view.findViewById(R.id.date);
        refreshData();
    }

    /**
     * set the time
     *
     * @param tod
     */
    private void getTime(double tod) {
        int hours;
        int minutes;
        int seconds;
        String time;

        hours = (int) tod / 3600;
        minutes = (int) (tod % 3600) / 60;
        seconds = (int) ((tod % 3600) % 60);
        time = String.format("%02d", hours) + ":" + String.format("%02d", minutes) + ":" + String.format("%02d", seconds);

        timetxt.setText(time);
    }

    /**
     * refresh data
     */
    public void refreshData() {
        if (getActivity() == null)
        {
            return;
        }
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                int gpsSatellitesVisL1 = 0;
                int gpsSatellitesVisL5 = 0;
                int gpsSatellitesUsedL1 = 0;
                int gpsSatellitesUsedL5 = 0;
                int gpsSatellitesTrackL1 = 0;
                int gpsSatellitesTrackL5 = 0;

                int galSatellitesVisL1 = 0;
                int galSatellitesVisL5 = 0;
                int galSatellitesUsedL1 = 0;
                int galSatellitesUsedL5 = 0;
                int galSatellitesTrackL1 = 0;
                int galSatellitesTrackL5 = 0;

                int bdsSatellitesVisL1 = 0;
                int bdsSatellitesVisL5 = 0;
                int bdsSatellitesUsedL1 = 0;
                int bdsSatellitesUsedL5 = 0;
                int bdsSatellitesTrackL1 = 0;
                int bdsSatellitesTrackL5 = 0;

                int gloSatellitesVisL1 = 0;
                int gloSatellitesVisL5 = 0;
                int gloSatellitesUsedL1 = 0;
                int gloSatellitesUsedL5 = 0;
                int gloSatellitesTrackL1 = 0;
                int gloSatellitesTrackL5 = 0;

                double tod;                 //time of day
                Date date = Calendar.getInstance().getTime();
                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                dateFormat.setTimeZone(TimeZone.getTimeZone("gmt"));
                DecimalFormat cartesian = new DecimalFormat("#.###");
                DecimalFormat geographic = new DecimalFormat("#.########");
                DecimalFormat altitude = new DecimalFormat("#.###");
                DecimalFormat velocity = new DecimalFormat("#.#");

                //creating a bundle to receive data from main activity
                Bundle bundle;

                // Selecting trackable satellites
                //checking that arguments are not null
                if (getArguments() != null) {
                    //then we recuperate arguments in our bundle object
                    bundle = getArguments();

                    if (bundle.getSerializable("TrackedObservations") != null) {

                        HashMap<String, GNSSObservation>  gnssObservation = (HashMap<String, GNSSObservation>) bundle.getSerializable("TrackedObservations");

                        for (HashMap.Entry<String, GNSSObservation> entry : gnssObservation.entrySet()) {

                            GNSSObservation current = entry.getValue();

                            switch (current.getConstellation())
                            {
                                case GnssStatus.CONSTELLATION_GPS:
                                    if (current.getCn0L1()> 20) {
                                        gpsSatellitesTrackL1++;
                                    }
                                    if (current.getCn0L5()> 20) {
                                        gpsSatellitesTrackL5++;
                                    }
                                    break;
                                case GnssStatus.CONSTELLATION_GALILEO:
                                    if (current.getCn0L1()> 20) {
                                        galSatellitesTrackL1++;
                                    }
                                    if (current.getCn0L5()> 20){
                                        galSatellitesTrackL5++;
                                    }
                                    break;
                                case GnssStatus.CONSTELLATION_BEIDOU:
                                    if (current.getCn0L1()> 20) {
                                        bdsSatellitesTrackL1++;
                                    }
                                    if (current.getCn0L5()> 20){
                                        bdsSatellitesTrackL5++;
                                    }
                                    break;

                                case GnssStatus.CONSTELLATION_GLONASS:
                                    if (current.getCn0L1()> 20) {
                                        gloSatellitesTrackL1++;
                                    }
                                    if (current.getCn0L5()> 20){
                                        gloSatellitesTrackL5++;
                                    }
                                    break;
                                //default:
                                    //Log.i("Tracking", "Signal Unknown");

                            }
                        }
                    }

                    if (bundle.getParcelable("GnssMeasurementsEvent") != null) {
                        //recuperating object in a local variable
                        GnssMeasurementsEvent mGnssMeasurementsEvent = bundle.getParcelable("GnssMeasurementsEvent");
                        mCoordinates = (Coordinates) bundle.getSerializable("ComputedPosition");
                        tod = bundle.getDouble("Time");
                        double gdop = bundle.getDouble("GDOP");

                        //recuperating gnss clock from measurement
                        GnssClock mGnssClock = mGnssMeasurementsEvent.getClock();
                        //creating list to save all satellites measurements
                        List<GnssMeasurement> mListMeasurements = new ArrayList<GnssMeasurement>();
                        //adding all satellite measurements to our list
                        mListMeasurements.addAll(mGnssMeasurementsEvent.getMeasurements());
                        //we create an iterator to our list
                        ListIterator<GnssMeasurement> itListMeasures = mListMeasurements.listIterator();

                        //counting satellite number by constellation
                        while (itListMeasures.hasNext()) {
                            GnssMeasurement current = itListMeasures.next();
                            //Log.i(">> FREQ", "" + current.getCarrierFrequencyHz());
                            switch (current.getConstellationType()) {
                                case GnssStatus.CONSTELLATION_GPS:

                                    if (current.getCarrierFrequencyHz() > 1.545e9 && current.getCarrierFrequencyHz() < 1.605e9) {
                                        gpsSatellitesVisL1++;
                                    }
                                    else if (current.getCarrierFrequencyHz() > 1.146e9 && current.getCarrierFrequencyHz() < 1.208e9) {
                                        gpsSatellitesVisL5++;
                                    }
                                    /*else {
                                        Log.i("Availability", "Acquired GPS signal out of range");
                                        //gpsSatellitesVisL1++;
                                    }*/
                                    break;

                                case GnssStatus.CONSTELLATION_GALILEO:

                                    if (current.getCarrierFrequencyHz() > 1.545e9 && current.getCarrierFrequencyHz() < 1.605e9) {
                                        galSatellitesVisL1++;
                                    }
                                    else if (current.getCarrierFrequencyHz() > 1.146e9 && current.getCarrierFrequencyHz() < 1.208e9) {
                                        galSatellitesVisL5++;
                                    }
                                    /*else{
                                        Log.i("Availability", "Acquired Galileo signal out of range");
                                        //galSatellitesVisL1++;
                                    }*/
                                    break;

                                case GnssStatus.CONSTELLATION_BEIDOU:

                                    if (current.getCarrierFrequencyHz() > 1.545e9 && current.getCarrierFrequencyHz() < 1.605e9) {
                                        bdsSatellitesVisL1++;
                                    }
                                    else if (current.getCarrierFrequencyHz() > 1.146e9 && current.getCarrierFrequencyHz() < 1.208e9) {
                                        bdsSatellitesVisL5++;
                                    }
                                    /*else{
                                        Log.i("Availability", "Acquired Beidou signal out of range");
                                        //bdsSatellitesVisL1++;
                                    }*/
                                    break;
                                case GnssStatus.CONSTELLATION_GLONASS:

                                    if (current.getCarrierFrequencyHz() > 1.545e9 && current.getCarrierFrequencyHz() < 1.605e9) {
                                        gloSatellitesVisL1++;
                                    }
                                    else if (current.getCarrierFrequencyHz() > 1.146e9 && current.getCarrierFrequencyHz() < 1.208e9) {
                                        gloSatellitesVisL5++;
                                    }
                                    /*else{
                                        Log.i("Availability", "Acquired GLONASS signal out of range");
                                        //gloSatellitesVisL1++;
                                    }*/
                                    break;
                                //default:
                                    //Log.i("Availability", "Signal Unknown");
                            }
                        }

                        //setting number of visible and trackable satellites
                        gpsVisL1.setText(String.valueOf(gpsSatellitesVisL1));
                        gpsVisL5.setText(String.valueOf(gpsSatellitesVisL5));
                        gpsTrackL1.setText(String.valueOf(gpsSatellitesTrackL1));
                        gpsTrackL5.setText(String.valueOf(gpsSatellitesTrackL5));
                        galVisL1.setText(String.valueOf(galSatellitesVisL1));
                        galVisL5.setText(String.valueOf(galSatellitesVisL5));
                        galTrackL1.setText(String.valueOf(galSatellitesTrackL1));
                        galTrackL5.setText(String.valueOf(galSatellitesTrackL5));
                        bdsVisL1.setText(String.valueOf(bdsSatellitesVisL1));
                        bdsVisL5.setText(String.valueOf(bdsSatellitesVisL5));
                        bdsTrackL1.setText(String.valueOf(bdsSatellitesTrackL1));
                        bdsTrackL5.setText(String.valueOf(bdsSatellitesTrackL5));
                        gloVisL1.setText(String.valueOf(gloSatellitesVisL1));
                        gloVisL5.setText(String.valueOf(gloSatellitesVisL5));
                        gloTrackL1.setText(String.valueOf(gloSatellitesTrackL1));
                        gloTrackL5.setText(String.valueOf(gloSatellitesTrackL5));

                        //setting time and date
                        getTime(tod);
                        datetxt.setText(dateFormat.format(date));

                        //setting cartesian and geographic coordinates
                        if (mCoordinates.getX() != 0) {
                            coordX.setText(String.valueOf(cartesian.format(mCoordinates.getX())));
                            coordY.setText(String.valueOf(cartesian.format(mCoordinates.getY())));
                            coordZ.setText(String.valueOf(cartesian.format(mCoordinates.getZ())));
                            coordLat.setText(String.valueOf(geographic.format(mCoordinates.getLatLngAlt().getLatitude())));
                            coordLon.setText(String.valueOf(geographic.format(mCoordinates.getLatLngAlt().getLongitude())));
                            coordAlt.setText(String.valueOf(altitude.format(mCoordinates.getLatLngAlt().getAltitude())));
                            userVelocity.setText(String.valueOf(velocity.format(mCoordinates.getVelocity())));
                            eVel.setText(String.valueOf(velocity.format(mCoordinates.geteDot())));
                            nVel.setText(String.valueOf(velocity.format(mCoordinates.getnDot())));
                            uVel.setText(String.valueOf(velocity.format(mCoordinates.getuDot())));
                            gdopView.setText(String.valueOf(velocity.format(gdop)));
                        }
                    }

                    if(bundle.getSerializable("GnssObservations") != null)
                    {
                        HashMap<String, GNSSObservation> gnssObservations;
                        gnssObservations = (HashMap<String, GNSSObservation>) bundle.getSerializable("GnssObservations");

                        for (HashMap.Entry<String, GNSSObservation> entry : gnssObservations.entrySet()) {
                            GNSSObservation current = entry.getValue();

                            switch (current.getConstellation())
                            {
                                case GnssStatus.CONSTELLATION_GPS:
                                    if (current.getPseudorangeL1() > 0) {
                                        gpsSatellitesUsedL1++;
                                    }
                                    if (current.getPseudorangeL5() > 0){
                                        gpsSatellitesUsedL5 ++;
                                    }
                                    break;
                                case GnssStatus.CONSTELLATION_GALILEO:
                                    if (current.getPseudorangeL1() > 0) {
                                        galSatellitesUsedL1++;
                                    }
                                    if (current.getPseudorangeL5() > 0){
                                        galSatellitesUsedL5 ++;
                                    }
                                    break;

                                case GnssStatus.CONSTELLATION_BEIDOU:
                                    if (current.getPseudorangeL1() > 0) {
                                        bdsSatellitesUsedL1++;
                                    }
                                    if (current.getPseudorangeL5() > 0){
                                        bdsSatellitesUsedL5 ++;
                                    }
                                    break;

                                case GnssStatus.CONSTELLATION_GLONASS:
                                    if (current.getPseudorangeL1() > 0) {
                                        gloSatellitesUsedL1++;
                                    }
                                    if (current.getPseudorangeL5() > 0){
                                        gloSatellitesUsedL5 ++;
                                    }
                                    break;
                                //default:
                                    //Log.i("Usability", "Signal Unusable");
                            }
                        }
                    }
                    gpsUsedL1.setText(String.valueOf(gpsSatellitesUsedL1));
                    gpsUsedL5.setText(String.valueOf(gpsSatellitesUsedL5));
                    galUsedL1.setText(String.valueOf(galSatellitesUsedL1));
                    galUsedL5.setText(String.valueOf(galSatellitesUsedL5));
                    bdsUsedL1.setText(String.valueOf(bdsSatellitesUsedL1));
                    bdsUsedL5.setText(String.valueOf(bdsSatellitesUsedL5));
                    gloUsedL1.setText(String.valueOf(gloSatellitesUsedL1));
                    gloUsedL5.setText(String.valueOf(gloSatellitesUsedL5));
                }
            }
        });
    }
}
