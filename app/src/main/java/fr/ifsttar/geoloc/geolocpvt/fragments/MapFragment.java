///=================================================================================================
// Class MainActivity
//      Author :  Jose Gilberto RESENDIZ FONSECA
// Modified by :  Antoine GRENIER - 2019/09/06
//        Date :  2019/09/06
///=================================================================================================
/*
 * Copyright 2018(c) IFSTTAR - TeamGEOLOC
 *
 * This file is part of the GeolocPVT application.
 *
 * GeolocPVT is distributed as a free software in order to build a community of users, contributors,
 * developers who will contribute to the project and ensure the necessary means for its evolution.
 *
 * GeolocPVT is a free software; you can redistribute it and/or modify it under the terms of the
 * GNU Lesser General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version. Any modification of source code in this
 * LGPL software must also be published under the LGPL license.
 *
 * GeolocPVT is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the GNU Lesser General Public License along with GeolocPVT.
 * If not, see <https://www.gnu.org/licenses/lgpl.txt/>.
 */
///=================================================================================================
package fr.ifsttar.geoloc.geolocpvt.fragments;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.preference.PreferenceManager;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.core.content.res.ResourcesCompat;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.osmdroid.api.IMapController;
import org.osmdroid.config.Configuration;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.Marker;

import android.location.LocationManager;
import android.location.Location;

import android.widget.Button;

import java.text.DecimalFormat;
import java.util.Objects;

import fr.ifsttar.geoloc.geoloclib.Coordinates;
import fr.ifsttar.geoloc.geolocpvt.R;
import fr.ifsttar.geoloc.geoloclib.LatLngAlt;

/**
 * Class MapFragment
 */
public class MapFragment extends Fragment implements Button.OnClickListener {

    private MapView mMapView;
    private IMapController mapController;
    private Marker marker;
    private boolean firstLoop;
    private LatLngAlt mLatLngAlt;

    @SuppressLint("InflateParams")
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //initializing osmdroid configuration
        Context ctx = getContext();
        Configuration.getInstance().load(ctx, PreferenceManager.getDefaultSharedPreferences(ctx));
        //creating the view for the fragment which contains all the visual information
        return inflater.inflate(R.layout.fragment_map, null);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mLatLngAlt = new LatLngAlt(47.2172, -1.5533, 0);
        firstLoop = true;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Button center = view.findViewById(R.id.buttonCenter);
        center.setOnClickListener(this);

        Button refresh = view.findViewById(R.id.buttonRefresh);
        refresh.setOnClickListener(this);

        mMapView = view.findViewById(R.id.mapView);
        mMapView.setTileSource(TileSourceFactory.MAPNIK);
        mMapView.setBuiltInZoomControls(true);
        mMapView.setMultiTouchControls(true);
        mapController = mMapView.getController();
        marker = new Marker(mMapView);
        mapController.setZoom(25);
        GeoPoint startPoint = new GeoPoint(mLatLngAlt.getLatitude(), mLatLngAlt.getLongitude());
        mapController.setCenter(startPoint);
        marker.setIcon(ResourcesCompat.getDrawable(getResources(), R.drawable.red_marker, null));
        marker.setPosition(startPoint);
        marker.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM);
        marker.setTitle("Nantes");
        mMapView.getOverlays().add(marker);
        refreshData();
    }

    /**
     * refresh the data
     */
    @SuppressLint("DefaultLocale")
    public void refreshData() {
        if (mMapView != null) {
            mMapView.invalidate();

            Marker newMarker = new Marker(mMapView);
            newMarker.setIcon(ResourcesCompat.getDrawable(Objects.requireNonNull(getResources()), R.drawable.red_marker, null));
            newMarker.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM);

            /*Marker refMarker = new Marker(mMapView);
            refMarker.setIcon(ResourcesCompat.getDrawable(Objects.requireNonNull(getResources()), R.drawable.red_marker, null));
            refMarker.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM);*/

            /*Marker adrMarker = new Marker(mMapView);
            adrMarker.setIcon(ResourcesCompat.getDrawable(Objects.requireNonNull(getResources()), R.drawable.b_marker, null));
            adrMarker.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM);*/

            if (getArguments() != null) {
                Bundle bundle;
                bundle = getArguments();
                if (getArguments().getDouble("Lon") != 0) {
                    mLatLngAlt = new LatLngAlt(bundle.getDouble("Lat"), bundle.getDouble("Lon"), 0);
                    newMarker.setTitle(String.format("Current position:\n %3.8f N, %3.8f E",
                            mLatLngAlt.getLatitude(), mLatLngAlt.getLongitude()));

                    newMarker.setPosition(new GeoPoint(mLatLngAlt.getLatitude(), mLatLngAlt.getLongitude()));
                    mMapView.getOverlays().add(newMarker);
                }

                /*if (bundle.getSerializable("ReferencePosition") != null) {
                    Coordinates refCoord = (Coordinates) bundle.getSerializable("ReferencePosition");
                    refMarker.setPosition(new GeoPoint(
                            refCoord.getLatLngAlt().getLatitude(),
                            refCoord.getLatLngAlt().getLongitude()));
                    mMapView.getOverlays().add(refMarker);
                }*/

                /*try
                {
                    LocationManager lm = (LocationManager) Objects.requireNonNull(getActivity()).getSystemService(Context.LOCATION_SERVICE);
                    Location adrLoc = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                    assert adrLoc != null;
                    adrMarker.setPosition(new GeoPoint(adrLoc.getLatitude(),adrLoc.getLongitude()));
                    mMapView.getOverlays().add(adrMarker);
                }
                catch (SecurityException e)
                {
                    Log.d("Location Access"," Permission denied by the user");
                }*/
            }

            if (firstLoop) {
                mapController.setCenter(new GeoPoint(mLatLngAlt.getLatitude(), mLatLngAlt.getLongitude()));
                firstLoop = false;
            }
        }

    }

    //----------------------------------------------------------------------------------------------

    /**
     * manage button to center to calculated point
     * @param v
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.buttonCenter:
                mapController.setCenter(new GeoPoint(mLatLngAlt.getLatitude(),mLatLngAlt.getLongitude()));
                break;
            case R.id.buttonRefresh:
                Log.e("MAP", "Refresh button clicked");
                mMapView.invalidate();
                mMapView.getOverlays().clear();
                break;
        }
    }

    //----------------------------------------------------------------------------------------------
}