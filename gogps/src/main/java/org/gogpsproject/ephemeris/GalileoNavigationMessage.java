package org.gogpsproject.ephemeris;

import android.location.GnssNavigationMessage;

import android.util.Log;
import java.util.Arrays;
import java.math.BigInteger;

public class GalileoNavigationMessage {

    private GnssNavigationMessage mGnssNavigationMessage;

    /**
     * mData: 30 bytes Navigation Message
     */
    private byte[] mData;

    private String binStr;

    GalileoNavigationMessage(GnssNavigationMessage mGnssNavigationMessage){
        this.mGnssNavigationMessage = mGnssNavigationMessage;
        mData = this.mGnssNavigationMessage.getData();
        //Log.d("GalileoNavigationMsg", Arrays.toString(mData));
        binStr = toBinaryStringFromByteArray(mData);
    }
    /**
     * Converts navigation byte data to bit string
     * @param b 30 bytes 1-D array
     * @return 128 characters (bit) string
     */
    private String toBinaryStringFromByteArray(byte[] b){

        //BigInteger intWord = new BigInteger(b);
        StringBuilder str = new StringBuilder();
        for (byte value : b) {
            String code = String.format("%8s", Integer.toBinaryString(value & 255)).replace(' ', '0');
            str.append(code);
        }

         String page = str.substring(0,239);
         String page1 = page.substring(2,114);
         String page2 = page.substring(122,138);
         return (page1 + page2);

    }
    public String getWord(int begin, int end){
        return binStr.substring(begin, end);
    }

    /*public char getChar(int idx){
        return binStr.charAt(idx);
    }*/
}